**Branch: develop**

Proyecto realizado en:

Back-end:

- Spring Boot 2 (2.1.4)
- Spring Data JPA

--------------
- REST
--------------

Front-end:

- Angular (8.2.10)
- Bootstrap (4.3.1)
- jQuery (3.4.1)

--------------
- Maven
--------------

Base de datos:

- MySQL: (8.0.15)